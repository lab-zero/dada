class Expression extends Nomad
{
        constructor(map = '')
        {
                super(map)
                this.previous = -1
                this.range = -1
                this.string = ''
        }
        
        get content()
        {
                return this.string
        }
        
        set content(string)
        {
                this.string += string
        }
        
        get position()
        {
                if (this.previous)
                        return this.range
                        
                return this.previous
        }

        set position(index)
        {
                if (this.previous)
                {
                        this.range[0] = this.previous
                        this.range[1] = index
                        this.previous = -1
                }
                else
                {
                        this.previous = index
                        this.range = -1
                }
        }
        
        process(text)
        {
                this.position = text.position
                
                if (this.range)
                        this.content = text.content.substring(this.previous, this.position + 1)
        }
}         