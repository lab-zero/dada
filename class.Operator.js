class Operator extends Nomad
{
        constructor(map = '')
        {
                super(map)
                
                this.opList =
                {
                        'Text: ['.']
                        'Expression': ['.', ',']
                        'Section': ['.', ',', ';', '_']
                }
                
                this.form = 'Text'
                this.separators = [' ', '!']
        }
        
        get content()
        {
                return this.form
        }
        
        process(text)
        {
                function findSeparators(index, text)
                {
                        let before = ' '
                        let after = ' '
                        
                        if (index > 0)
                                before = text.content[index - 1]
                
                        if (index < text.content.length - 1)
                                after = text.content[index + 1]
                        
                        if (!this.separators.includes(before) ||
                            !this.separators.includes(after))
                                return false
                }
                
                let operators = this.opList[this.form]
                let forms = Object.keys(this.opList)
                let frmsIndex = forms.indexOf(this.form)                          

                do
                {
                        let opsIndex = 0
                        
                        do
                        {
                                let index = text.content.indexOf(operators[opsIndex], text.position)
                                
                                if (index &&
                                    findSeparators(index, text))
                                {
                                        text.position = index
                                                
                                        if (opsIndex == frmsIndex &&
                                            this.form != 'Section')
                                                this.form = forms[index + 1]
                                        else
                                                this.form = forms[index]
                                        
                                        return true
                                }
                                
                                opsIndex++
                        }
                        while (opsIndex < operators.length)
                        
                        text.position++
                }
                while (index)
                
                text.position = -1
                                
                return false
        }
}