class Text extends Nomad
{
        constructor(code)
        {
                super('')
                this.code = code
                this.index = 0
        }
        
        get content()
        {
                return this.code
        }
        
        get position()
        {
                return this.index
        }
        
        set position(index)
        {
                this.index = index
        }
        
        process()
        {
        }
}