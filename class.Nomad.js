class Nomad
{
        constructor(map)
        {
                if (map == '')
                        return
                
                this.forms = map
                let elmsKeys = Object.keys(map)
                let frmsValues = Object.values(map)
                
                for (let index = 0; index < elmsKeys.length; index++)
                        this.elements[frmsValues[index]] = elmsKeys[index]
        }
        
        process(request)
        {
                let output = new [this.forms['output']]
                let input = new [this.forms['input']](request)
                let stream = new [this.forms['stream']]
                let filter = new [this.forms['filter']]                
                
                while (stream.process(input))
                {
                        [this.elements[stream.content]].process(input)
                        
                        if (this.elements[stream.content] == 'output')
                                alert(output.content)
        }
}